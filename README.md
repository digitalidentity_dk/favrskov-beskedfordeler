Proof-of-concept implementation of integration with Beskedfordeler, for submitting (and reading) events on Person objects

== Disclaimer
* Based partially on the sample code from KMD found here
   https://digitaliseringskataloget.dk/integration/sf1460c
* The sample code is not ready for production, and the proof-of-concept is by extension not ready for production either
* The proof-of-concept does not act on real events, but on simulated events, and more work is needed for production use
