package dk.kombit.token;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

public class TokenManager {
	private static final Logger LOGGER = LoggerFactory.getLogger(TokenManager.class);
	private Map<KeyPair, Token> cachedTokens = new HashMap<KeyPair, Token>();
	private String tokenServiceUrl;

	public TokenManager(String url) {
		tokenServiceUrl = url;
	}

	public String getToken(String cvr, String base64Certificate, String serviceEntityId) throws SAXException, IOException, ParserConfigurationException {
		KeyPair kp = new KeyPair(cvr, serviceEntityId);

		LOGGER.debug("Checking cache for token for: " + kp);
		synchronized (cachedTokens) {
			if (cachedTokens.containsKey(kp)) {
				Token token = cachedTokens.get(kp);
				if (isExpired(token)) {
					LOGGER.debug("Cache contains expired token for " + kp);
					cachedTokens.remove(kp);
				}
				else {
					LOGGER.debug("Cache contains valid token for " + kp);
					return token.value;
				}
			}
			else {
				LOGGER.debug("Cache does not contain token for " + kp);
			}
		}

		SfwClient client = new SfwClient(tokenServiceUrl);
		String value = client.performTokenCall(cvr, base64Certificate, serviceEntityId);
		synchronized (cachedTokens) {
			LOGGER.debug("Adding token for " + kp + " to cache");
			
			Token token = new Token();
			token.tts = LocalDateTime.now();
			token.value = value;

			cachedTokens.put(kp, token);
		}

		return value;
	}
	
	private boolean isExpired(Token token) {
		if (LocalDateTime.now().minusHours(4L).isAfter(token.tts)) {
			return true;
		}
		
		return false;
	}
	
	private static class Token {
		private LocalDateTime tts;
		private String value;
	}

	private static class KeyPair {
		public String v1;
		public String v2;

		public KeyPair(String v1, String v2) {
			this.v1 = v1;
			this.v2 = v2;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}

			if (!(o instanceof KeyPair)) {
				return false;
			}

			KeyPair key = (KeyPair) o;
			
			return v1 == key.v1 && v2 == key.v2;
		}

		@Override
		public int hashCode() {
			int result = v1.hashCode();
			result = 31 * result + v2.hashCode();
			return result;
		}

		@Override
		public String toString() {
			return "{cvr:" + v1 + ",entityId:" + v2 + "}";
		}
	}
}
