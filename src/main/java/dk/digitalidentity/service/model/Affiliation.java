package dk.digitalidentity.service.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Affiliation {
    private String uuid;
    private String startDate;
    private String stopDate;
    private boolean deleted;
    private String orgUnitUuid;
    private String positionName;
    private boolean prime;
}
