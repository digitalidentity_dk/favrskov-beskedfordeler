package dk.digitalidentity.service.model;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Person {
    private String uuid;
    private boolean deleted;
    private String firstname;
    private String surname;
    private Set<User> users;
    private Set<Affiliation> affiliations;
    
    public String getName() {
    	return firstname + " " + surname;
    }
}
