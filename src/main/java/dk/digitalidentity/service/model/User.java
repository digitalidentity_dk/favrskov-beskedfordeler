package dk.digitalidentity.service.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    private String uuid;
    private String userId;
    private String userType;
    private boolean prime;
}
