package dk.digitalidentity.service;

import java.io.IOException;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ReturnListener;

import dk.digitalidentity.controller.dto.Message;
import dk.digitalidentity.util.ServiceUtil;
import dk.kombit.bf.beskedkuvert.BeskedkuvertType;
import dk.kombit.bf.beskedkuvert.FiltreringsdataType;
import dk.kombit.bf.beskedkuvert.HaendelsesbeskedType;
import dk.kombit.bf.beskedkuvert.LeveranceinformationType;
import dk.kombit.bf.beskedkuvert.ObjektRegistreringType;
import dk.kombit.bf.beskedkuvert.RelateretObjektType;
import dk.kombit.token.TokenManager;
import dk.kombit.token.TokenSaslConfig;
import lombok.extern.slf4j.Slf4j;
import oio.sagdok._3_0.StandardReturType;
import oio.sagdok._3_0.TidspunktType;
import oio.sagdok._3_0.UnikIdType;
import oio.sts.organisation.wsdl.AktoerType;

@Slf4j
@Service
public class MessageBrokerService {
	public final QName HAENDELSESBESKED_QNAME = new QName("urn:oio:besked:kuvert:1.0", "Haendelsesbesked");
	public final String URN_OIO_CVRNR_PREFIX = "urn:oio:cvr-nr:";
	public final String BESKEDFORDELER_RABBITMQ_VIRTUAL_HOST = "BF";
	public final String PUBLISH_EXCHANGE_NAME = "AFSEND_BESKED_EXCHANGE";
	public final String DISTRIBUTION_QUEUE_NAME = "";
	public final String PUBLISH_REPLY_QUEUE = "amq.rabbitmq.reply-to";

	private TokenManager tokenManager;
	private String decodedToken;
	private String token;
	private Channel channel;
	private Connection conn;
    private QueueingConsumer consumer;

	@Autowired
	private ServiceUtil serviceUtil;

	@Value("${request.cvr}")
	public String requestCVRNumber;

	@Value("${request.anvendersystemId}")
	private String anvendersystemId;

	@Value("${beskedfordeler.dueslagId}")
    private String dueslagId;

	@Value("${beskedfordeler.hostname}")
	public String beskedfordelerHostname;
	
	@Value("${beskedfordeler.port}")
	public int beskedfordelerPortnumber;
	
	@Value("${beskedfordeler.reply_timeout_msecs}")
	private int replyTimeoutMsecs;

	@Value("${sts.url}")
	public String stsRESTUrl;

	@Value("${sts.afhent.entityid}")
	public String beskedfordelerAfhentServiceURI;
	
	@Value("${sts.afsend.entityid}")
	public String beskedfordelerAfsendServiceURI;

	@PostConstruct
	private void init() {
		try {
			tokenManager = new TokenManager(stsRESTUrl);
		}
		catch (Exception e) {
			log.error("Caught exception initializing token manager", e);
			return;
		}

		serviceUtil.setupSsl();
	}

	public void sendMessage(String eventType, String personUuid, String userUuid, String functionUuid, String orgunitUuid) throws Exception {
		log.info("main: Fetching token...");
		fetchToken(beskedfordelerAfsendServiceURI);

		log.info("main: Building message...");
		HaendelsesbeskedType haendelsesbesked = generateMessage(eventType, functionUuid, orgunitUuid, personUuid, userUuid);

		setupMessage(haendelsesbesked);

		log.info("main: Opening connection...");
		openConnection();

		log.info("main: Sending message...");
		publishMessage(haendelsesbesked);

		log.info("main: Closing connection...");
		closeConnection();
	}

	public List<Message> readMessages() throws Exception {
		log.info("dueslag = " + dueslagId);
    	
    	log.info("main: Fetching token...");
        fetchToken(beskedfordelerAfhentServiceURI);

    	log.info("main: Opening connection...");
    	openConnection();
        openReadConnection();

    	log.info("main: Processing messages...");
        List<Message> result = processMessages();

    	log.info("main: Closing connection...");
        closeConnection();
        
    	log.info("main: Exit time: "+(new Date().toString()));

    	return result;
	}
	
	private UnikIdType getUuidReference(String uuid) {
		UnikIdType ref = new UnikIdType();
		ref.setUUIDIdentifikator(uuid);

		return ref;
	}

	private UnikIdType getUrnReference(String urn) {
		UnikIdType ref = new UnikIdType();
		ref.setURNIdentifikator(urn);

		return ref;
	}

	private HaendelsesbeskedType generateMessage(String eventTypeUuid, String organisationFunktionUuid, String orgUnitUuid, String personUuid, String userUuid) throws Exception {
		UnikIdType beskedId = getUuidReference(UUID.randomUUID().toString());
		UnikIdType objektRegistreringsId = getUuidReference(UUID.randomUUID().toString());

		// OrgBruger beskedtypen
		UnikIdType beskedType = getUuidReference("94ef3c82-10e9-4d12-a9d9-7a7242d5ba83");

		// Ikke fortrolige data
		UnikIdType openDataReference = getUuidReference("1d81c472-0808-44cc-963d-f5ef0170ae1d");

		// Favrskov Kommune
		UnikIdType kommuneId = getUrnReference("urn:oio:cvr-nr:29189714");

		// OrganisationFunktion reference
		UnikIdType organisationFunktionReferenceType = getUuidReference("c66a5d12-8582-4415-b988-85ed5519e7d6");
		UnikIdType organisationFunktionReferenceUuid = getUuidReference(organisationFunktionUuid);
		UnikIdType organisationFunktionReferenceRolle = getUuidReference("02e61900-33e0-407f-a2a7-22f70221f003"); // enheds rolle

		// OrgUnit reference
		UnikIdType orgUnitReferenceType = getUuidReference("c4cc1906-a30f-41e2-b3c2-61ccabbd83b7");
		UnikIdType orgUnitReferenceUuid = getUuidReference(orgUnitUuid);

		// Person reference
		UnikIdType personReferenceType = getUuidReference("ce7fcf97-a8a2-447a-8690-d38aa0f6e23c");
		UnikIdType personReferenceUuid = getUuidReference(personUuid);

		// User reference
		UnikIdType userReferenceType = getUuidReference("2b808769-8d09-4aa3-ac28-f8ac3526010f");
		UnikIdType userReferenceUuid = getUuidReference(userUuid);

        /*  
        BrugerOprettet	6ce72dab-1e09-425e-8503-5d6522f72fdf
        BrugerAendret	862f6d4e-f70a-4e27-9383-24bb32919cf0
        BrugerNedlagt	9edb0c34-36e2-4d87-aba8-3ca7ade718ee
        BrugerTilknyttetOrgEnhed	0fcf7d06-7258-4b9e-90f0-b9af0d484d29
        BrugerFjernetOrgEnhed	588a59a3-6ee4-4b6f-b423-5b082f5d290e
        */
		UnikIdType objektHandling = new UnikIdType();
		objektHandling.setUUIDIdentifikator(eventTypeUuid);

		// TODO: probably our it-systems UUID
		AktoerType ansvarligAktoer = new AktoerType();
		ansvarligAktoer.setUUIDIdentifikator("39c4fbbe-3ada-43c2-ba7e-076bef7cbec1");

		// who did it (random UUID, because we don't actually know)
		AktoerType registreringsAktoer = new AktoerType();
		registreringsAktoer.setUUIDIdentifikator(UUID.randomUUID().toString());

		final GregorianCalendar now = new GregorianCalendar();
		TidspunktType nowTts = new TidspunktType();
		nowTts.setTidsstempelDatoTid(DatatypeFactory.newInstance().newXMLGregorianCalendar(now));
		
		ObjektRegistreringType objektRegistrering = new ObjektRegistreringType();
		objektRegistrering.setObjektRegistreringId(objektRegistreringsId);
		objektRegistrering.setRegistreringsAktoer(registreringsAktoer);
		objektRegistrering.setRegistreringstidspunkt(nowTts);
		objektRegistrering.setObjektAnsvarligMyndighed(kommuneId);
		objektRegistrering.setObjektId(userReferenceUuid);
		objektRegistrering.setObjektType(userReferenceType);
		objektRegistrering.setObjektHandling(objektHandling);
		
		RelateretObjektType orgFunctionReference = new RelateretObjektType();
		orgFunctionReference.setObjektId(organisationFunktionReferenceUuid);
		orgFunctionReference.setObjektType(organisationFunktionReferenceType);
		orgFunctionReference.setObjektRolle(organisationFunktionReferenceRolle);
		
		RelateretObjektType orgUnitReference = new RelateretObjektType();
		orgUnitReference.setObjektId(orgUnitReferenceUuid);
		orgUnitReference.setObjektType(orgUnitReferenceType);
		
		RelateretObjektType personReference = new RelateretObjektType();
		personReference.setObjektId(personReferenceUuid);
		personReference.setObjektType(personReferenceType);
		
		FiltreringsdataType filtreringsData = new FiltreringsdataType();
		filtreringsData.setBeskedtype(beskedType);
		filtreringsData.setBeskedAnsvarligAktoer(ansvarligAktoer);
		filtreringsData.getTilladtModtager().add(kommuneId);
		filtreringsData.getRelateretObjekt().add(orgFunctionReference);
		filtreringsData.getRelateretObjekt().add(orgUnitReference);
		filtreringsData.getRelateretObjekt().add(personReference);
		filtreringsData.getObjektRegistrering().add(objektRegistrering);
		
		LeveranceinformationType leveranceInformation = new LeveranceinformationType();
		leveranceInformation.setDannelsestidspunkt(nowTts);
		leveranceInformation.setSikkerhedsklassificering(openDataReference);
		
		TidspunktType gyldighedTil = new TidspunktType();
		gyldighedTil.setGraenseIndikator(true);
		
		BeskedkuvertType kuvert = new BeskedkuvertType();
		kuvert.setFiltreringsdata(filtreringsData);
		kuvert.setLeveranceinformation(leveranceInformation);
		kuvert.setGyldighedFra(nowTts);
		kuvert.setGyldighedTil(gyldighedTil);
		
		HaendelsesbeskedType msg = new HaendelsesbeskedType();
		msg.setBeskedId(beskedId);
		msg.setBeskedVersion("1.0");
		msg.setBeskedkuvert(kuvert);

		return msg;
	}
	
	private List<Message> processMessages() throws Exception {
		List<Message> result = new ArrayList<>();

        try {
        	// attempt to read 3 messages 
			for (int i = 0; i < 3; i++) {
				log.info("main: Waiting for messages...");
				
				Message message = handleNextMessage();
				if (message == null) {
					break;
				}

				result.add(message);
			}
		}
        catch (Exception e) {
			log.error("Caught exception while handling messages",e);
		}
        
        return result;
	}
	
	private Message handleNextMessage() throws Exception {
		try {
            // Wait for message on queue
            QueueingConsumer.Delivery delivery = consumer.nextDelivery(replyTimeoutMsecs);
            if (delivery == null) {
				log.info("handleNextMessage(): Received no reply on queue " + dueslagId + " within " + replyTimeoutMsecs + " ms, retrying");
				return null;
            }
            
            if (delivery.getProperties() == null) {
				log.warn("handleNextMessage(): Received reply on queue " + dueslagId + " with no properties, retrying");
            	return null;
            }

            String transactionId = delivery.getProperties().getMessageId();
            
            // Convert XML from message to haendelsesbesked object
            String messageString = new String(delivery.getBody(), "UTF-8");
			log.debug("handleNextMessage(): Received message body\n" + messageString);
            
            // Could catch unmarshal error and place message on error queue
            HaendelsesbeskedType haendelsesbesked = (HaendelsesbeskedType)serviceUtil.unmarshal(HaendelsesbeskedType.class, messageString);
            
            String beskedId = haendelsesbesked.getBeskedId().getUUIDIdentifikator();

			boolean multiple = false;
            long deliveryTag = delivery.getEnvelope().getDeliveryTag();
			if (beskedId == null || "".equals(beskedId)) {
            	log.warn("handleNextMessage(): Received reply on queue "+dueslagId+" with no besked id (transaction id = "+transactionId+", delivery tag "+deliveryTag+"). NACK'ing message");

            	boolean requeue=false;
            	channel.basicNack(deliveryTag, multiple, requeue);
            } else {
            	log.debug("handleNextMessage(): Message received with NEW (unprocessed) beskedId: " + beskedId);                	
                
        		String besked = serviceUtil.marshal(haendelsesbesked, HAENDELSESBESKED_QNAME);
        		log.info("handleNextMessage(): Got message");

        		Message message = new Message();
        		message.setBrief(besked.substring(0, (besked.length() > 200) ? 200 : besked.length()) + "...");
        		message.setMessage(serviceUtil.prettyPrintXML(besked));
        		message.setTts(new Date());
        		
            	log.info("handleNextMessage(): Acknowledging beskedId: " + beskedId + " (transaction id = "+transactionId+", delivery tag "+deliveryTag+").");	                	
                channel.basicAck(deliveryTag, multiple);
                
                return message;
            }

			return null;

        } catch (IOException e) {
        	log.error("Caught exception while processing message",e);
        	
            // If network exceptions happens we will reconnect and retry
            reopenConnection();
        }

        // Return false to stop handling more messages
        return null;
    }
	
	private void setupMessage(HaendelsesbeskedType haendelsesbesked) throws DatatypeConfigurationException {
		try {
			String besked1 = serviceUtil.marshal(haendelsesbesked, HAENDELSESBESKED_QNAME);
			log.debug("Message before setup:\n" + serviceUtil.prettyPrintXML(besked1));

			// ensure our anvendersystemId is set up in message (so sender == this sender)
			FiltreringsdataType filtreringsdata = haendelsesbesked.getBeskedkuvert().getFiltreringsdata();
			if (filtreringsdata != null) {
				AktoerType beskedAnsvarligAktoer = filtreringsdata.getBeskedAnsvarligAktoer();
				if (beskedAnsvarligAktoer != null) {
					String uuid = beskedAnsvarligAktoer.getUUIDIdentifikator();
					if (anvendersystemId != null && (uuid == null || !anvendersystemId.equals(uuid))) {
						log.warn("Warning: Anvendersystem was " + anvendersystemId + " and message UUID has different value " + uuid);
					}
					if (anvendersystemId != null) {
						beskedAnsvarligAktoer.setUUIDIdentifikator(anvendersystemId);
					}
				}
			}

			// ensure that CVR number is present in TilladtModtager and ObjektAnsvarligMyndighed
			String cvr = URN_OIO_CVRNR_PREFIX + requestCVRNumber;
			List<UnikIdType> tilladtModtager = filtreringsdata.getTilladtModtager();
			List<ObjektRegistreringType> objektRegistrering = filtreringsdata.getObjektRegistrering();

			if (tilladtModtager == null || tilladtModtager.isEmpty()) {
				log.warn("Warning: no TilladtModtager in message");
			} else {
				if (tilladtModtager.size() > 1) {
					log.warn("Warning: multiple TilladtModtager in message");
				}
				boolean found = false;
				for (UnikIdType uid : tilladtModtager) {
					if (cvr.equals(uid.getURNIdentifikator())) {
						found = true;
						break;
					}
				}
				if (!found) {
					log.warn("Warning: TilladtModtager " + cvr + " not in message");
				}
			}
			if (objektRegistrering == null || objektRegistrering.isEmpty()) {
				log.warn("Warning: no ObjektRegistrering in message");
			} else if (objektRegistrering.size() > 1) {
				log.warn("Warning: more than 1 ObjektRegistrering in message");
			} else if (!cvr.equals(objektRegistrering.get(0).getObjektAnsvarligMyndighed().getURNIdentifikator())) {
				log.error("ERROR: ObjektRegistrering.ObjektAnsvarligMyndighed in message not set to " + cvr + ", publish will fail");
				throw new RuntimeException("CVR number " + cvr + " not equal to municipiality id in message: " + objektRegistrering.get(0).getObjektAnsvarligMyndighed().getURNIdentifikator());
			}

			// always set dannelsestidspunkt to now
			TidspunktType tt = new TidspunktType();
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(new Date());
			tt.setTidsstempelDatoTid(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
			haendelsesbesked.getBeskedkuvert().getLeveranceinformation().setDannelsestidspunkt(tt);
			besked1 = serviceUtil.marshal(haendelsesbesked, HAENDELSESBESKED_QNAME);
			log.debug("Message after setup:\n" + serviceUtil.prettyPrintXML(besked1));
		} catch (Exception e) {
			log.error("Caught exception while setting up message", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Publishes the supplied message, retrying {@value #NUMBER_OF_PUBLISH_RETRIES} times.
	 *
	 * @param haendelsesbesked
	 * @throws Exception
	 */
	private void publishMessage(HaendelsesbeskedType haendelsesbesked) throws Exception {
		// result of sending the message
		boolean success = false;

		// one transactionId for whole transaction
		String transactionId = UUID.randomUUID().toString();

		log.info("publishMessage: Publising message with transactionId=" + transactionId);

		// try 3 times, then give up
		for (int i = 0; i < 3; i++) {
			try {
				// Setup details for message properties
				String corrId = UUID.randomUUID().toString(); // CorrelationId for the RPC-call, used to pair with request and response

				// Add security-token to the message header
				Map<String, Object> headers = new HashMap<String, Object>();
				headers.put("token", decodedToken);

				// Build message properties object
				AMQP.BasicProperties props = new AMQP.BasicProperties
						.Builder()
						.correlationId(corrId)
						// reply to property have to be set to point to pseudo queue associated with the channel (Direct Reply method)
						.replyTo(PUBLISH_REPLY_QUEUE)
						// custom headers: token have to be passed to the processing service in message header
						.headers(headers)
						.messageId(transactionId)
						.build();

				log.debug("publishMessage:attempt " + (1 + i) + ": Publishing message with transactionId=" + transactionId + " correlationId=" + corrId);

				// Convert input object to XML-string for the RPC-call
				String inputString = serviceUtil.marshal(haendelsesbesked, HAENDELSESBESKED_QNAME);

				// Setup consumer to listen for the RPC-reply
				QueueingConsumer consumer = new QueueingConsumer(channel);
				channel.basicConsume(PUBLISH_REPLY_QUEUE, true, consumer);

				// A listener there checks for silent errors
				channel.addReturnListener(new ReturnListener() {
					public void handleReturn(int replyCode, String replyText, String exchange, String routingKey, AMQP.BasicProperties properties, byte[] body) throws IOException {
						log.error("The message was silently dropped, with reply code: " + replyCode + " exchange: " + exchange + " routingKey: " + routingKey);
					}
				});

				log.debug("Publishing data:\n" + inputString);

				// Send the RPC-request using AMQP
				channel.basicPublish(PUBLISH_EXCHANGE_NAME, DISTRIBUTION_QUEUE_NAME, true, false, props, inputString.getBytes(Charset.forName("UTF-8")));

				// Wait for the RPC-reply (matching on correlationId)
				String outputString = "";
				boolean replyReceived = false;
				while (true) {
					QueueingConsumer.Delivery delivery = consumer.nextDelivery(replyTimeoutMsecs);
					if (delivery == null) {
						break;
					}
					BasicProperties replyProperties = delivery.getProperties();
					log.debug("Received reply for request with correlation id " + replyProperties.getCorrelationId());
					if (replyProperties.getCorrelationId().equals(corrId)) {
						outputString = new String(delivery.getBody(), "UTF-8");
						replyReceived = true;
						log.debug("Received reply:\n***\n" + outputString + "\n***");
						break;
					}
				}

				if (!replyReceived) {
					log.info("Received no reply from consumer within timeout of " + replyTimeoutMsecs + " ms, retrying sending");
					reopenConnection();
					continue;
				}

				// Convert RPC-reply from XML-string to output object
				StandardReturType output = serviceUtil.unmarshal(StandardReturType.class, outputString);

				// Handle the RPC-reply
				int statusKode = output.getStatusKode().intValue();
				if (statusKode == 20) {
					// Service executed successfully
					log.info("Publish executed successfully");

					success = true;
					break;
				} else {
					// Service returned other status code
					log.error("Call returned status code: " + statusKode + " - " + output.getFejlbeskedTekst());

					// Handle error according to the external systems scenario
					break;
				}
			} catch (SocketException se) {
				log.warn("Caught exception: ", se);
				reopenConnection();
			} catch (IOException e) {
				// If network exceptions happens we will reconnect and retry
				log.warn("Caught exception: ", e);
				reopenConnection();
			}
		}

		if (success) {
			log.info("Message sent sucessfully.");
		} else {
			log.error("Failed to send message after 3 retries.");
		}
	}
	
	/**
	 * Fetches a token for sending messages
	 *
	 * @throws SAXException                 thrown if errors occur while fetching token
	 * @throws IOException                  thrown if errors occur while fetching token
	 * @throws ParserConfigurationException thrown if errors occur while fetching token
	 */
	private void fetchToken(String endpoint) throws SAXException, IOException, ParserConfigurationException {
		log.info("Getting token for CVR=" + requestCVRNumber + " to service=" + beskedfordelerAfsendServiceURI);
		String certificate = "";
		try {
			certificate = serviceUtil.getCertificate();
		} catch (Exception e) {
			log.error("Caught exception while getting certificate", e);
			throw new RuntimeException(e);
		}
		token = tokenManager.getToken(requestCVRNumber, certificate, endpoint);
		decodedToken = new String(Base64.getDecoder().decode(token));
		log.info("Token received");
		log.debug("Got token: " + decodedToken);
	}
	
	private void openConnection() throws Exception {
		// Setup AMQP connection
		log.debug("Opening connection to AMQP host on " + beskedfordelerHostname + ":" + beskedfordelerPortnumber);

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(beskedfordelerHostname);
		factory.setPort(beskedfordelerPortnumber);

		SSLContext sc = SSLContext.getInstance("TLS");
		TrustManager[] tm = serviceUtil.getTrustManagers();
		KeyManager[] km = serviceUtil.getKeyManagers();

		sc.init(km, tm, new java.security.SecureRandom());
		factory.useSslProtocol(sc);

		factory.setVirtualHost(BESKEDFORDELER_RABBITMQ_VIRTUAL_HOST);

		// Setup SASL config using security-token
		factory.setSaslConfig(new TokenSaslConfig(decodedToken));

		// Open AMQP connection
		conn = factory.newConnection();
		channel = conn.createChannel();
	}
	
    private void openReadConnection() throws Exception {
		log.info("Connecting to queue " + dueslagId);
        
        consumer = new QueueingConsumer(channel);
        boolean autoAck = false;
        channel.basicConsume(dueslagId, autoAck, consumer);
    }

	/**
	 * Closes channel and connection to AMQP
	 */
	private void closeConnection() {
		// Close AMPQ connection when no used anymore (reuse same connection for multiple messages for performance)
		log.debug("Closing connection");
		try {
			channel.close();
			conn.close();
		} catch (Exception e) {
			log.warn("Caught exception closing connection", e);
		}
	}

	/**
	 * Closes and reopens connection to AMQP.
	 */
	private void reopenConnection() throws Exception {
		log.debug("Reopening connection");
		closeConnection();
		openConnection();
	}
}
