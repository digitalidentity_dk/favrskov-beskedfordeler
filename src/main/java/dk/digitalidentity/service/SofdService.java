package dk.digitalidentity.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import dk.digitalidentity.service.model.Person;
import dk.digitalidentity.service.model.PersonsEmbedded;
import lombok.extern.slf4j.Slf4j;

@EnableCaching
@Service
@Slf4j
public class SofdService {
	private static final int pageSize = 500;

	@Value("${sofd.url}")
	private String url;

	@Value("${sofd.apiKey}")
	private String apiKey;

    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private SofdService self;
    
    @PostConstruct
    public void init() {
    	try {
    		log.info("Fetching all persons from SOFD");
    		List<Person> persons = self.getPersons();
    		log.info("Fetched " + persons.size() + " persons");
    	}
    	catch (Exception ex) {
    		log.error("Failed to get persons from SOFD", ex);
    	}
    }
    
    @Cacheable(value = "persons")
	public List<Person> getPersons() throws Exception {
		HttpEntity<String> request = new HttpEntity<>(getHeaders());

		String query = "/v2/persons?size=" + pageSize;
		List<Person> result = new ArrayList<>();

		long page = 0;
		boolean empty = false;

		do {
			ResponseEntity<PersonsEmbedded> response = restTemplate.exchange(url + query + "&page=" + page, HttpMethod.GET, request, PersonsEmbedded.class);
			if (!response.getStatusCode().equals(HttpStatus.OK)) {
				throw new Exception("Failed to fetch a list of Persons. " + response.getStatusCodeValue() + ", response=" + response.getBody());
			}
			
			result.addAll(response.getBody().getPersons());

			empty = (response.getBody().getPersons().size() == 0);			
			page += 1;
			
			// TODO: if we want to read all, do not break here
			break;
		}
		while (!empty);
		
		return result.stream()
				.filter(p -> !p.isDeleted() &&
							 !StringUtils.isEmpty(p.getFirstname()) &&
							 !StringUtils.isEmpty(p.getSurname()) &&
							 p.getAffiliations().stream().anyMatch(a -> a.isPrime()) &&
							 p.getUsers().stream().anyMatch(u -> u.getUserType().equals("ACTIVE_DIRECTORY") && u.isPrime())
						)
				.collect(Collectors.toList());
	}
	
    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("apiKey", apiKey);
        headers.add("Content-Type", "application/json");

        return headers;
    }
}
