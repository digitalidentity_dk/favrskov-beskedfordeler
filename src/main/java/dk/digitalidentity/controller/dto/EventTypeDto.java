package dk.digitalidentity.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventTypeDto {
	private String eventType;
}
