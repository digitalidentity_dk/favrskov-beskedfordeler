package dk.digitalidentity.controller.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {
	private String id;
	private Date tts;
	private String brief;
	private String message;
}
