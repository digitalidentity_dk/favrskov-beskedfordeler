package dk.digitalidentity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import dk.digitalidentity.service.SofdService;

@Controller
public class IndexController {

	@Autowired
	private SofdService sofdService;

	@GetMapping("/")
	public String index(Model model) throws Exception {
		model.addAttribute("persons", sofdService.getPersons());

		return "index";
	}
}
