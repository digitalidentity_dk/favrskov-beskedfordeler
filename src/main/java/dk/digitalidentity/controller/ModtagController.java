package dk.digitalidentity.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import dk.digitalidentity.controller.dto.Message;
import dk.digitalidentity.service.MessageBrokerService;

@Controller
public class ModtagController {
	private static Map<String, Message> map = new HashMap<>();
	private static long maxId = 0;
	
	@Autowired
	private MessageBrokerService messageBrokerService;

	@GetMapping("/modtag")
	public String modtag(Model model) throws Exception {
		List<Message> messages = messageBrokerService.readMessages();
		for (Message message : messages) {
			maxId++;
			message.setId(Long.toString(maxId));
			map.put(message.getId(), message);
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DAY_OF_MONTH, -1);
		Date oneDayAgo = cal.getTime();

		for (String key : map.keySet()) {
			Message message = map.get(key);
			
			if (message.getTts().before(oneDayAgo)) {
				map.remove(key);
			}
		}
		
		model.addAttribute("messages", map.values());

		return "modtag";
	}
	
	@GetMapping("/modtag/{id}")
	public String modtagBesked(Model model, @PathVariable("id") String id) throws Exception {
		Message message = map.get(id);
		
		model.addAttribute("message", message);

		return "besked";
	}	
}
