package dk.digitalidentity.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import dk.digitalidentity.controller.dto.EventTypeDto;
import dk.digitalidentity.service.MessageBrokerService;
import dk.digitalidentity.service.SofdService;
import dk.digitalidentity.service.model.Person;

@Controller
public class MessageEventController {

	@Autowired
	private SofdService sofdService;

	@Autowired
	private MessageBrokerService messageBrokerService;

	@GetMapping("/event/{uuid}")
	public String get(Model model, @PathVariable("uuid") String uuid) throws Exception {
		return "event";
	}
	
	@PostMapping("/event/{uuid}")
	public String post(Model model, @PathVariable("uuid") String uuid, @ModelAttribute("payload") EventTypeDto eventTypeDto) throws Exception {
		String personUuid = uuid;
		String userUuid = null;
		String functionUuid = null;
		String orgUnitUuid = null;

		for (Person person : sofdService.getPersons()) {
			if (person.getUuid().equals(uuid)) {
				userUuid = person.getUsers().stream().filter(u -> u.isPrime() && u.getUserType().equals("ACTIVE_DIRECTORY")).findFirst().get().getUuid();
				functionUuid = person.getAffiliations().stream().filter(a -> a.isPrime()).findFirst().get().getUuid();
				orgUnitUuid = person.getAffiliations().stream().filter(a -> a.isPrime()).findFirst().get().getOrgUnitUuid();
				break;
			}
		}

		messageBrokerService.sendMessage(eventTypeDto.getEventType(), personUuid, userUuid, functionUuid, orgUnitUuid);
		
		return "redirect:/";
	}
}
