package dk.digitalidentity.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.security.KeyStore;
import java.util.Base64;
import java.util.Enumeration;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ServiceUtil {

	@Value("${request.keyStoreFile}")
	public String keyStoreFile;

	@Value("${request.keyStorePassword}")
	public String keyStorePassword;

	@Value("${request.trustStoreFile}")
	public String trustStoreFile;

	@Value("${request.trustStorePassword}")
	public String trustStorePassword;

	public void setupSsl() {
		log.info("Keystore file: " + keyStoreFile);
		System.setProperty("javax.net.ssl.keyStore", keyStoreFile);
		System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);

		log.info("Truststore file: " + trustStoreFile);
		System.setProperty("javax.net.ssl.trustStore", trustStoreFile);
		System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
	}

	public String getCertificate() {
		try {
			KeyStore ksKeys = createKeyStoreForKeys();
			Enumeration<String> aliases = ksKeys.aliases();
			while (aliases.hasMoreElements()) {
				String alias = aliases.nextElement();
				byte[] certificateEncodedBytes = ksKeys.getCertificate(alias).getEncoded();
				return new String(Base64.getEncoder().encode(certificateEncodedBytes));
			}
		}
		catch (Exception e) {
			log.error("Caught exception while instantiating keystore", e);
			throw new RuntimeException(e);
		}

		throw new RuntimeException("No certificate found in " + keyStoreFile);
	}

	public KeyManager[] getKeyManagers() {
		try {
			KeyStore ksKeys = createKeyStoreForKeys();
			// KeyManagers decide which key material to use
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(ksKeys, keyStorePassword.toCharArray());
			return kmf.getKeyManagers();
		}
		catch (Exception e) {
			log.error("Caught exception while instantiating keystore", e);
			throw new RuntimeException(e);
		}
	}

	private KeyStore createKeyStoreForKeys() {
		try {
			// First initialize the key and trust material
			KeyStore ksKeys = KeyStore.getInstance("JKS");
			char[] passphrase = keyStorePassword.toCharArray();

			ksKeys.load(new FileInputStream(ResourceUtils.getFile(keyStoreFile)), passphrase);
			return ksKeys;
		}
		catch (Exception e) {
			log.error("Caught exception while instantiating keystore", e);
			throw new RuntimeException(e);
		}
	}

	public <T> T unmarshal(Class<T> cl, String s) throws JAXBException {
		return unmarshal(cl, new StringReader(s));
	}

	public <T> T unmarshal(Class<T> cl, Reader r) throws JAXBException {
		return unmarshal(cl, new StreamSource(r));
	}

	public <T> T unmarshal(Class<T> cl, Source s) throws JAXBException {
		JAXBContext ctx = JAXBContext.newInstance(cl);
		Unmarshaller u = ctx.createUnmarshaller();
		return u.unmarshal(s, cl).getValue();
	}

	public <T> String marshal(T obj, QName rootName) throws JAXBException {
		StringWriter sw = new StringWriter();
		marshal(obj, sw, rootName);

		return sw.toString();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T> void marshal(T obj, Writer wr, QName rootName) throws JAXBException {
		JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
		Marshaller m = ctx.createMarshaller();
		JAXBElement<?> jaxbElement = new JAXBElement(rootName, obj.getClass(), obj);
		m.marshal(jaxbElement, wr);
	}

	public String prettyPrintXML(String xmlDocument) {
		try {
			DocumentBuilderFactory dbFactory;
			DocumentBuilder dBuilder;
			Document original = null;

			try {
				dbFactory = DocumentBuilderFactory.newInstance();
				dbFactory.setNamespaceAware(true);
				dBuilder = dbFactory.newDocumentBuilder();
				original = dBuilder.parse(new InputSource(new StringReader(xmlDocument)));
			}
			catch (SAXException | IOException | ParserConfigurationException e) {
				e.printStackTrace();
			}
			
			StringWriter stringWriter = new StringWriter();
			StreamResult xmlOutput = new StreamResult(stringWriter);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.transform(new DOMSource(original), xmlOutput);
			
			return xmlOutput.getWriter().toString();
		}
		catch (Exception ex) {
			throw new RuntimeException("Error converting to String", ex);
		}
	}

	public TrustManager[] getTrustManagers() {
		TrustManagerFactory tmf = null;

		try {
			tmf = TrustManagerFactory.getInstance("SunX509", "SunJSSE");
		}
		catch (Exception e) {
			log.error("Caught exception while instantiating TrustManagerFactory", e);
			throw new RuntimeException(e);
		}
		
		try {
			KeyStore tsKeys = KeyStore.getInstance("JKS");
			char[] passphrase = trustStorePassword.toCharArray();

			tsKeys.load(new FileInputStream(ResourceUtils.getFile(trustStoreFile)), passphrase);
			tmf.init(tsKeys);
		}
		catch (Exception e) {
			log.error("Caught exception while initializing TrustManagerFactory", e);
			throw new RuntimeException(e);
		}

		return tmf.getTrustManagers();
	}

}
