#!/bin/bash
mvn install:install-file -Dfile=libs/sts-bf-soap-1.0-SNAPSHOT.jar -DgroupId=dk.kombit -DartifactId=sts-bf-soap -Dversion=1.0-SNAPSHOT -Dpackaging=jar
mvn install:install-file -Dfile=libs/sts-commons-safewhere-1.0-SNAPSHOT.jar -DgroupId=dk.kombit -DartifactId=sts-commons-safewhere -Dversion=1.0-SNAPSHOT -Dpackaging=jar
mvn install:install-file -Dfile=libs/sts-commons-xjb-2.0-SNAPSHOT.jar -DgroupId=dk.kombit -DartifactId=sts-commons-xjb -Dversion=2.0-SNAPSHOT -Dpackaging=jar
mvn install:install-file -Dfile=libs/sts-interface-2.2-SNAPSHOT.jar -DgroupId=dk.kombit -DartifactId=sts-interface -Dversion=2.2-SNAPSHOT -Dpackaging=jar
